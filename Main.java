public class Main {

    public static void main(String[] args) {
        //== compares references
        //equals compares values
        //Create obj text1 in string pool
        String text1 = "text";
        //Don't create obj text2 because it is a reference to text1
        String text2 = "text";
        if (text1 == text2) {
            System.out.println("The same text from string pool.");
        }
        //Create obj text3 in local memory
        String text3 = new String("text");
        //Create obj text4 in local memory
        String text4 = new String("text");
        if (text3 == text4) {
            System.out.println("The same text from local memory.");
        }
        if (text3.equals(text4)) {
            System.out.println("The same text from local memory with equals.");
        }
        if (text3.equals(text1)) {
            System.out.println("The same text from local and string pool with equals.");
        }
    }
}
